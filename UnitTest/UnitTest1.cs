using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApp1;

namespace UnitTestt1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            int k = 8;
            int A = 15;
            int x0 = 42;
            int time = 34;

            int result = 924;
            Calculation example = new Calculation();
            int actual = example.function(k, A, x0, time);
            Assert.AreEqual(result, actual);
        }

        [TestMethod]
        public void TestMethod2()
        {
            int k = 2;
            int A = 3;
            int x0 = 4;
            int time = 5;

            int result = 360;
            Calculation example = new Calculation();
            int actual = example.function(k, A, x0, time);
            Assert.AreEqual(result, actual);
        }

        [TestMethod]
        public void TestMethod3()
        {
            int k = 10;
            int A = 4;
            int x0 = 7;
            int time = 20;

            int result = 133;
            Calculation example = new Calculation();
            int actual = example.function(k, A, x0, time);
            Assert.AreEqual(result, actual);
        }
    }
}
