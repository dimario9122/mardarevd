﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Calculation
    {
        public string name;
        public int result = 0;
        public int function(int k, int A, int x0, int time)
        {
            if (time > 60)
            {
                k = 0;
                result = x0 * (A * A * A + k * (k + 1)) / 4;
            }
            else
            {
                if ((A > 0.1) && (A < 5.5))
                {
                    k = k / 2;
                    A = 2 + k - 23;
                    result = x0 * (A * A + k * (A - 20)) / 4;
                }
                else
                {
                    A = A - 1;
                    k = k + 100;
                    result = x0 * (A * A + k * (A - 15)) / 4;
                }
            }
            return result;
        }

        public void GetInfo()
        {
            Console.WriteLine($"Название функции: {name} \n Результат: {result}");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Calculation example = new Calculation();
            example.name = "Приблизительное вычисление скорости точек тела при параллельном движении";
            example.function(10, 4, 7, 20);
            example.GetInfo();
            Console.ReadKey();
        }
    }
}
